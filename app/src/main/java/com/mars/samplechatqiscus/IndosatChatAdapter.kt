package com.mars.samplechatqiscus

import android.content.Context
import com.qiscus.sdk.ui.adapter.QiscusChatAdapter
import android.view.ViewGroup
import com.qiscus.sdk.chat.core.data.model.QiscusComment
import com.qiscus.sdk.ui.adapter.viewholder.QiscusBaseMessageViewHolder
import org.json.JSONException



class IndosatChatAdapter(context: Context?, group: Boolean) : QiscusChatAdapter(context, group) {

    private val TYPE_MESSAGE_EMPTY = 2335

    companion object {
        val PAYLOAD_TYPE_EMPTY = "PAYLOAD_TYPE_EMPTY"
    }

    override fun getItemViewTypeMyMessage(qiscusComment: QiscusComment?, position: Int): Int {
        try {
            val payload = qiscusComment?.extras
            if (payload?.optString("TYPE") == PAYLOAD_TYPE_EMPTY) {
                return TYPE_MESSAGE_EMPTY
            }
        } catch (ignored: JSONException) {
            ignored.message
        }

        return super.getItemViewTypeMyMessage(qiscusComment, position)
    }

    override fun getItemViewTypeOthersMessage(qiscusComment: QiscusComment?, position: Int): Int {
        return super.getItemViewTypeOthersMessage(qiscusComment, position)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        when (viewType) {
            TYPE_MESSAGE_EMPTY -> return R.layout.item_message_empty_system
            else -> return super.getItemResourceLayout(viewType)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): QiscusBaseMessageViewHolder<QiscusComment> {
        when (viewType) {
            TYPE_MESSAGE_EMPTY -> return EmptySystemEventViewHolder(getView(parent, viewType),
                    itemClickListener, longItemClickListener)
            else -> return super.onCreateViewHolder(parent, viewType)
        }
    }
}
