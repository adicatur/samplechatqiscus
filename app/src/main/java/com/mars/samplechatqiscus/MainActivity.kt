package com.mars.samplechatqiscus

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.qiscus.sdk.chat.core.data.model.QiscusAccount
import com.qiscus.sdk.chat.core.QiscusCore
import com.qiscus.sdk.Qiscus
import org.json.JSONException
import android.util.Log
import android.widget.Button
import android.widget.EditText
import org.json.JSONObject
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


class MainActivity : AppCompatActivity() {
    private lateinit var btnStartChat: Button
    private lateinit var etUserId: EditText
    private lateinit var etUserKey: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnStartChat = findViewById(R.id.btn_start_chat) as Button;

        etUserId = findViewById(R.id.editText) as EditText
        etUserKey = findViewById(R.id.editText2) as EditText
        btnStartChat.setOnClickListener({setUser()})
    }

    private fun setUser() {
        var userId = etUserId.text.toString().trim()
        var userKey = etUserKey.text.toString().trim()

        Qiscus.setUser(userId, userKey)
                .withUsername(userId)
                .save(object : QiscusCore.SetUserListener {
                    override fun onSuccess(qiscusAccount: QiscusAccount) {
                        //updateUserAvatar(userName);
                        initChat()
                    }

                    override fun onError(throwable: Throwable) {
                        //error
                        print(throwable.stackTrace)
                    }
                })
    }

    private fun initChat() {
        val payload = JSONObject()
        try {
            payload.put("MSISDN", "12345678")
            payload.put("USERNAME", "adicatur08")
            payload.put("TYPE", IndosatChatAdapter.PAYLOAD_TYPE_EMPTY)
            Log.i("MainActivity", "Qiscus: payload generated: " + payload.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Qiscus.buildChatRoomWith("indosatbot")
                .build()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map({ qiscusChatRoom ->
                    IndosatChatActivity.generateIntent(
                            this, qiscusChatRoom, payload)
                })
                .subscribe({ intent ->
                    startActivity(intent)

                }) {
                    it.printStackTrace()
                } }


    }
