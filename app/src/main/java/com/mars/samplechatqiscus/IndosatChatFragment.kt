package com.mars.samplechatqiscus

import com.qiscus.sdk.ui.fragment.QiscusChatFragment
import com.qiscus.sdk.ui.adapter.QiscusChatAdapter
import android.os.Bundle
import android.view.View
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom



class IndosatChatFragment : QiscusChatFragment() {

    companion object {

        fun newInstance(qiscusChatRoom: QiscusChatRoom): IndosatChatFragment {
            val fragment = IndosatChatFragment()
            val bundle = Bundle()
            bundle.putParcelable(CHAT_ROOM_DATA, qiscusChatRoom)
            fragment.arguments = bundle
            return fragment
        }
    }

    // requestFilePermission
    private fun requestAccessFilePermission() {
        requestAddFilePermission()
    }

    // requestCameraPermission
    private fun requestAccessCameraPermission() {
        requestCameraPermission()
    }

    // requestLocationPermission
    private fun requestAccessLocationPermission() {
        requestAddLocationPermission()
    }


    override fun getResourceLayout(): Int {
        return R.layout.fragment_qiscus_chat
    }

    override fun onLoadView(view: View) {
        super.onLoadView(view)
    }

    override fun onCreateChatComponents(savedInstanceState: Bundle?) {
        super.onCreateChatComponents(savedInstanceState)
    }

    override fun onCreateChatAdapter(): QiscusChatAdapter {
        return IndosatChatAdapter(activity, qiscusChatRoom.isGroup)
    }
}
