package com.mars.samplechatqiscus;

import android.app.Application;

import com.qiscus.sdk.Qiscus;

public class SampleApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Qiscus.init(this,  "yourAPPID");

        Qiscus.getChatConfig()
                .setEnableRequestPermission(false) // disable request all permission, enable manually each permission
                .setLeftBubbleColor(R.color.colorPrimary)
                .setRightBubbleColor(R.color.qiscus_accent)
                .setEnablePushNotification(true)
                .setOnlyEnablePushNotificationOutsideChatRoom(true)
                .setNotificationBuilderInterceptor(
                        new IndosatChatNotificationBuilder());

    }
}
