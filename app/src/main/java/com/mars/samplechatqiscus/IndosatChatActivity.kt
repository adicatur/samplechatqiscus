package com.mars.samplechatqiscus

import android.content.Context
import com.qiscus.sdk.ui.QiscusBaseChatActivity
import java.util.*
import com.qiscus.sdk.chat.core.data.model.QiscusComment
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import com.qiscus.sdk.chat.core.data.remote.QiscusApi
import com.qiscus.sdk.Qiscus
import android.os.Bundle
import android.content.Intent
import android.util.Log
import android.view.MenuItem
import org.json.JSONObject
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom



class IndosatChatActivity : QiscusBaseChatActivity() {

    private val TAG = "IndosatChat"


    companion object {

        private var mPayload: JSONObject? = null
        private var mChatRoomId: Long = 0

        fun generateIntent(context: Context, qiscusChatRoom: QiscusChatRoom,
                           payload: JSONObject): Intent {
            val intent = Intent(context, IndosatChatActivity::class.java)
            intent.putExtra(QiscusBaseChatActivity.CHAT_ROOM_DATA, qiscusChatRoom)

            mPayload = payload
            mChatRoomId = qiscusChatRoom.id
            return intent
        }
    }


    override fun onSetStatusBarColor() {

    }

    override fun getResourceLayout(): Int {
        return R.layout.activity_chat
    }

    override fun onLoadView() {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() === android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateChatFragment(): IndosatChatFragment {
        return IndosatChatFragment.newInstance(qiscusChatRoom)
    }

    override fun onViewReady(savedInstanceState: Bundle?) {
        super.onViewReady(savedInstanceState)

        sendInitComment()
    }

    override fun onUserStatusChanged(user: String, online: Boolean, lastActive: Date) {

    }

    override fun onUserTyping(user: String, typing: Boolean) {

    }

    public override fun onDestroy() {
        if (Qiscus.hasSetupUser()) {
            Qiscus.clearUser()
        }
        super.onDestroy()
    }

    private fun sendInitComment() {
        //check whether local data whether exist or not the messages (empty or not)
        var comments = Qiscus.getDataStore().getComments(qiscusChatRoom.id)
        if (comments.size > 0) {
            //if have local data then clear in server and local by calling this API
            var roomIds = listOf(qiscusChatRoom.id)
            QiscusApi.getInstance().clearCommentsByRoomIds(roomIds)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe ({
                      Log.d("This" , "delete success")
                    }){
                        it.printStackTrace()
                    }
        }

        //post comment
//        val initComment = QiscusComment.generateMessage(mChatRoomId, "Hi")
//        initComment.extras = mPayload;
//
//        QiscusApi.getInstance().postComment(initComment)
//                .doOnSubscribe({ Qiscus.getDataStore().addOrUpdate(qiscusChatRoom)} ) //update comment to local database
//                .doOnNext({ updateStateOnQiscus(it) })
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe({ Log.i(TAG, "Qiscus: empty message with payload sent.") })
//                {
//                  it.printStackTrace()
//         }

    }

    private fun updateStateOnQiscus(comment : QiscusComment) {
        comment.state= QiscusComment.STATE_ON_QISCUS

        var savedQiscusComment = Qiscus.getDataStore().getComment(comment.uniqueId.toString())
        if (savedQiscusComment != null && savedQiscusComment.getState() > comment.getState()) {
            comment.setState(savedQiscusComment.getState());
        }

        //update comment to local database
        Qiscus.getDataStore().addOrUpdate(comment)
    }
}
