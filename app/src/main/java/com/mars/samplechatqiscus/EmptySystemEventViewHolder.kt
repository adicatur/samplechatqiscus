package com.mars.samplechatqiscus

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.qiscus.sdk.chat.core.data.model.QiscusComment
import com.qiscus.sdk.ui.adapter.OnItemClickListener
import com.qiscus.sdk.ui.adapter.OnLongItemClickListener
import com.qiscus.sdk.ui.adapter.viewholder.QiscusBaseMessageViewHolder

class EmptySystemEventViewHolder(view: View?, itemClickListener: OnItemClickListener?,
                                 longItemClickListener: OnLongItemClickListener?)
    : QiscusBaseMessageViewHolder<QiscusComment>(view, itemClickListener, longItemClickListener) {


    override fun getFirstMessageBubbleIndicatorView(itemView: View): ImageView? {
        return null
    }

    override fun getMessageBubbleView(itemView: View): View {
        return itemView.findViewById(R.id.message)
    }

    override fun getDateView(itemView: View): TextView? {
        return null
    }

    override fun getTimeView(itemView: View): TextView? {
        return null
    }

    override fun getMessageStateIndicatorView(itemView: View): ImageView? {
        return null
    }

    override fun getAvatarView(itemView: View): ImageView? {
        return null
    }

    override fun getSenderNameView(itemView: View): TextView? {
        return null
    }

    override fun showMessage(qiscusComment: QiscusComment) {

    }
}
