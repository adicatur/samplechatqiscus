package com.mars.samplechatqiscus;

import android.support.v4.app.NotificationCompat;

import com.qiscus.sdk.chat.core.data.model.QiscusComment;
import com.qiscus.sdk.data.model.QiscusNotificationBuilderInterceptor;

public class IndosatChatNotificationBuilder implements QiscusNotificationBuilderInterceptor {

    @Override
    public boolean intercept(NotificationCompat.Builder notificationBuilder, QiscusComment qiscusComment) {
        return false;
    }
}
